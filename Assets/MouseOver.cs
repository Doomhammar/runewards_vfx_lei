﻿using UnityEngine;
using System.Collections;

public class MouseOver : MonoBehaviour
{

    public GameObject card;

   

    void OnMouseEnter()
    {
        card.transform.localScale = new Vector3(5f, 5f, 5f);
        card.GetComponent<SpriteRenderer>().sortingOrder = 7;
    }

    void OnMouseExit()
    {
        card.transform.localScale = new Vector3(3f, 3f, 3f);
        card.GetComponent<SpriteRenderer>().sortingOrder = 2;
    }
}
